Name:           mmix
Version:        20131017
Release:        1%{?dist}
Summary:        MMIX simulators, assembler, and tools
License:        Knuth
URL:            http://mmix.cs.hm.edu/
Source0:        http://mmix.cs.hm.edu/src/mmix-%{version}.tgz
# CWEB "patches" to fix various GCC warnings
Source100:      https://raw.githubusercontent.com/ascherer/mmix/local/abstime.ch
Source101:      https://raw.githubusercontent.com/ascherer/mmix/local/mmix-arith.ch
Source102:      https://raw.githubusercontent.com/ascherer/mmix/local/mmix-config.ch
Source103:      https://raw.githubusercontent.com/ascherer/mmix/local/mmix-io.ch
Source104:      https://raw.githubusercontent.com/ascherer/mmix/local/mmix-pipe.ch
Source105:      https://raw.githubusercontent.com/ascherer/mmix/local/mmix-sim.ch
Source106:      https://raw.githubusercontent.com/ascherer/mmix/local/mmixal.ch
# CWEB "patches" to add features
Source200:      http://mmix.cs.hm.edu/tools/mmoimg/mmoimg.ch

BuildRequires:  tex-amsfonts
BuildRequires:  tex-cm
BuildRequires:  tex-cweb
BuildRequires:  tex-epsf
BuildRequires:  tex-etex
BuildRequires:  tex-knuth-lib
BuildRequires:  tex-pdftex
BuildRequires:  tex-tex
BuildRequires:  tex-texconfig

%description
MMIX is a computer intended to illustrate machine-level aspects of
programming. In the books The Art of Computer Programming, it replaces
MIX, the 1960s-style machine that formerly played such a role.  MMIX was
designed so that its machine language would be simple, elegant, and easy
to learn. At the same time it includes all of the complexities needed to
achieve high performance in practice, so that MMIX could in principle be
built and even perhaps be competitive with some of the fastest
general-purpose computers in the marketplace.

%prep
%setup -q -c -a 0
cp %{SOURCE100} %{SOURCE101} %{SOURCE102} %{SOURCE103} %{SOURCE104} \
   %{SOURCE105} %{SOURCE106} %{SOURCE200} .


%build
ctangle mmotype.w mmoimg.ch mmoimg.c
cweave mmotype.w mmoimg.ch mmoimg.tex

make all mmoimg CFLAGS="$RPM_OPT_FLAGS"

for f in mm*.w; do
  make ${f%.w}.pdf PDFTEX=pdftex
done
pdftex mmoimg.tex


%check
./mmixal -x -b 250 -l copy.mml copy.mms
./mmix copy copy.mms > copy.out
diff -u copy.mms copy.out

./mmixal -x -b 250 -l hello.mml hello.mms
./mmix -Dhello.mmb hello.mmo
echo -e '10000\nq' | ./mmmix plain.mmconfig hello.mmb

./mmixal -x -b 250 -l silly.mml silly.mms
./mmix -Dsilly.mmb silly.mmo
echo -e '10000\nq' | ./mmmix plain.mmconfig silly.mmb


%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install mmix mmixal mmmix mmotype mmoimg $RPM_BUILD_ROOT%{_bindir}/


%files
%doc README *.pdf
%{_bindir}/mmix
%{_bindir}/mmixal
%{_bindir}/mmmix
%{_bindir}/mmotype
%{_bindir}/mmoimg


%changelog
* Tue Aug 18 2015 Yaakov Selkowitz <yselkowi@redhat.com>
- Initial release
